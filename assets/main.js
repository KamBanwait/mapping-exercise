/*
  notes on implementation:

  WHEN A PAGE LOADS
  I want the map to load to central london by default

  WHAT USER CAN DO
  I want the user to enter a location in a input field and then click/submit a form to search using Google's API

  EXPECTED RESULTS
  I want some indication of loading to the user's interaction
  If many results are returned then they will appear the under search input
    (^ tweaked as I went throught to only show only when more than 1 result is returned - user can click on one of the search results to update the map with correct location)

  When the map renders at any time the details are logged in to the console
*/

// Get a location, the default is set to 'Central London' if no location is passed (default for page load)
function getLocation (location = 'central london') {
  const API_KEY = 'AIzaSyCspMg9VzAGgftPlo9CI28BNi_xhcQeJOs'

  // Using JS Fetch to call google and handle result or errors
  fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${location}&key=${API_KEY}`)
    .then(function (response) {
      // get response and conver to JSON
      return response.json()
    })
    .then(function (response) {
      // take JSON format and setup options to pass to rendermap and to server logs
      // TODO: refine what's being logged
      const results = response.results
      const options = {
        info: results[0],
        center: {
          lat: results[0].geometry.location.lat,
          lng: results[0].geometry.location.lng
        }
      }

      // update search results list with details of returned result
      renderResultsList(results)

      // render the map
      renderMap(options)

      // remove loading from fieldset
      toggleLoading(false)

      // send all detaila to log
      // TODO: Look into cleaner passing of args where not all args are present
      logToServer('', location, results, options)
    })
    .catch(function (error) {
      // TODO: using console to log error - shoud show to user
      console.error('There has been a problem', error)
      toggleLoading(false)
    })
}

// create a new map on load and when searched.
function renderMap (options) {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: options.zoom || 10, // set zoom to 10 of no options passed
    center: options.center || { lat: 51.5073509, lng: -0.1277583 } // london if no options passed
  })

  // sets up marker - provide default lat/lng if no options passed
  var marker = new google.maps.Marker({
    position: options.center || { lat: 51.5073509, lng: -0.1277583 },
    map: map
  })

  // info window content for when user clicks the marker
  var infowindow = new google.maps.InfoWindow({
    content: `
      <h5 class="title is-5">Latitude & Longitude</h5>
      <p class="subtitle is-6">
        lat: ${options.info.geometry.location.lat}<br />
        lng: ${options.info.geometry.location.lng}
      </p>
      <p>${options.info.formatted_address}</p>
    `
  })

  // zoom listener to log zoom change in console
  map.addListener('zoom_changed', function () {
    const zoomLevel = `Zoom level changed: ${map.getZoom()}`
    logToServer(zoomLevel)
  })

  // click listener to create info window
  marker.addListener('click', function () {
    infowindow.open(map, marker)
  })
}

function renderResultsList (results) {
  // select OL from dom to populate with results
  const orderedList = document.getElementById('searchResults')
  // empty string for <li> to populate when mapping results > 2
  let orderedListItems = ''

  // if results returned is 1 or less then set empty list
  if (results.length <= 1) { orderedList.innerHTML = ''; return }

  // ES6 map to go loop each location and pick out specific info needed for results list
  results.map(location => {
    orderedListItems += `
      <li class="searchResults-location" data-address="${location.formatted_address}">
        ${location.formatted_address}
      </li>
    `
  })

  orderedList.innerHTML = orderedListItems
}

function toggleLoading (status) {
  // disables form when requesting map data
  const fieldset = document.getElementById('searchFormFieldset')
  status ? fieldset.disabled = true : fieldset.disabled = false
}

// function to log details to server
// TODO: use desctructuring to handle args passed if nothing is passed
function logToServer (zoom = 'no change in zoom', location = '', results = '', options = '') {
  console.log('Zoom changed:', zoom)
  console.log('Search term:', location)
  console.log('All results:', results[0])
  console.log('Location:', options)

  // toggle the notification message
  showNotification()
}

// toggled notification
// TODO: needs to styled better to stop page jump
function showNotification () {
  const notification = document.getElementById('notification')

  notification.classList.remove('is-hidden')

  // automatically hide the notification after 3 seconds
  setTimeout(() => {
    notification.classList.add('is-hidden')
  }, 3000)
}

/*
  For both the doc event listeners:
  On any document submit & click we check for the ID of the form/button
  I match the class/id and then decide if it's going to do anything for the user
*/
document.addEventListener('click', function (event) {
  // Listen out for click events on the DOM and then only act if target class matches
  if (!event.target.matches('.searchResults-location')) return
  const location = event.target.getAttribute('data-address')
  // location should be availble, disable form whilst loading data
  toggleLoading(true)
  // request location data
  getLocation(location)
})

document.addEventListener('submit', function (event) {
  event.preventDefault()
  if (!event.target.matches('#searchForLocation')) return
  const location = document.getElementById('userLocation').value
  // if location is empty then exit and don't do anything
  if (!location) return

  // if location; disable form whilst loading data
  toggleLoading(true)
  // request location data
  getLocation(location)
})
