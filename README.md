**Mapping Excercise**

You can download the files locally and just run it via the index.hmtl file in a browser from your machine.

JS Availble via Netlify: https://modest-raman-22d8db.netlify.com
VueJS Availble via Netlify: https://ecstatic-bohr-dad965.netlify.com

JS:
[![Netlify Status](https://api.netlify.com/api/v1/badges/88bde2a3-e40e-4de2-af8a-fc4fa79d316d/deploy-status)](https://app.netlify.com/sites/modest-raman-22d8db/deploys)

---

VueJs:
[![Netlify Status](https://api.netlify.com/api/v1/badges/c92d49bb-c8d4-456c-9af1-ede471df6355/deploy-status)](https://app.netlify.com/sites/ecstatic-bohr-dad965/deploys)
_Time taken: roughly 3 1/2 hours_

---

I've used:

1. Bulma.io for the styling

2. Google's MAP & Geocode API

3. No framework for JS has been used

4. ESLint setup locally in VSCode for JS, HTML & CSS

---

Steps taken:

1. I created a simple page layout with Bulma. Thinking about the data possibly returned from Google. there could be more than 1 location returned and the user would need to choose the correct location

2. I started off with 1st rendering a map on the page without any user input. This shows the map load Central London. I added defaults to the parameters I send to Google after implementing the submit function from the user input.

3. User's search is handled with a form submission where I take the value of the input field and send that to Google. I take their response, via fetch. The response from Google is turned in to JSON

4. I've created an object for the results to be used in the search results list, if there's more than 1 response, and for the logs to the "server" (logs to console)

5. I've aimed to use functions to do one thing where possible. These could be further cleaned up to do less, I feel

---

Nice to have:

1. I've approached this with a not so strict TDD mindset but would like to have introduced test cases before writing the code

2. ~Use VueJS to manage the state of loading and messages in a better and cleaner implementation~ The VueJs version is implemented here: https://ecstatic-bohr-dad965.netlify.com

3. Clean up some of the layout and order of the JS

4. Need to add logging for _panning_ interatctions

5. Add correct user error message on page when fetch returns an error